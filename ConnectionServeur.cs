﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets

namespace La_bataille_du_café
{

    class ConnectionServeur
    {
        private string host;
        private int port;
        private Socket serveur;

        //Constructeur de la class
        public ConnectionServeur( string host, int port )
        {
            this.host = host;
            this.port = port;
            //Création d'un socket en TCP
            serveur = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        //Connection au serveur avec l'ip et le port
        public void Connection()
        {
            Console.WriteLine("Connection en cours");
            try
            {
                //Connection au serveur
                serveur.Connect(host, port);
            }
            catch { Console.WriteLine("Connection impossible"); }
            
            Console.WriteLine("Connection ok");
        }

        //Recuperation de la carte envoyé par le serveur
        public byte[] RecupCarte()
        {
            byte[] carte = new byte[300];

            //Fonction pour recevoir la carte
            int reçoi = serveur.Receive(carte);
          

            return carte;
        }

        
        

        //Deconnexion du serveur
        public void Déconnexion()
        {
            serveur.Close();
        }


        public Socket GetSocket()
        {
            return this.serveur;
        }

        public string GetIP()
        {
            return this.host;
        }

        public int Getport()
        {
            return this.port;
        }

        public void sendcoup(int x,int y)
        {
            string coup = "A:" + x + y;
            serveur.Send(Encoding.UTF8.GetBytes(coup));
        }

        //Méthode permettant de récupérer le coup du serveur
        public string coupserveur()
        {
            byte[] coup = new byte[4];

            //Méthode pour recevoir la carte
            int reçoi = serveur.Receive(coup);

            return Encoding.UTF8.GetString(coup);

        }

        //Méthode permettant de récupérer les scores envoyés par le serveur
        public string resultat()
        {
            byte[] coup = new byte[7];

            //Méthode pour recevoir la carte
            int reçoi = serveur.Receive(coup);

            return Encoding.UTF8.GetString(coup);

        }

    }
}
