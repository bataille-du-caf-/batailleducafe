﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace La_bataille_du_café
{
    
    class Carte
    {
        private string cartebrute; //Contient la carte envoyé par le serveur
        private int[] carteint; //Contient la carte découpé
        private char[,] cartedecode; //Contient la carte décodé

        private Dictionary<char, int> conteneurparcelle = new Dictionary<char, int>();

        public Carte(byte[] carteArrive)
        {
            //On transforme la carte en byte que le serv envois pour la mettre dans carteArrive
            this.cartebrute = Encoding.UTF8.GetString(carteArrive); 
            this.carteint = new int[CompteurCaractere(this.cartebrute)];
            this.cartedecode = new char[10,10];
        }

        //Découpage de la carte. On découpe la carte qui est un string en un tableau de int car les frontières sont données à travers des int.
        public void DecoupageCarte()
        {
            int index = 0;
            int indexint = 0;
            cartebrute = ":" + cartebrute;

            //Parcour de la carte donnée par le serv (cartebrute)
            foreach (char caractere in cartebrute)
            {
                if (Char.IsNumber(caractere) == true) //Si le caractere est bien un nombre
                {
                    if (Char.IsNumber(cartebrute[index + 1]) == true)
                    {
                        carteint[indexint] = (cartebrute[index] - 48) * 10 + (cartebrute[index + 1] - 48);
                        indexint++;
                    }
                    else if ((Char.IsNumber(cartebrute[index + 1])) == false && (Char.IsNumber(cartebrute[index - 1]) == false))
                    {
                        carteint[indexint] = cartebrute[index] - 48;
                        indexint++;
                    }
                }
                index++;
            }
            
        }

        //Fonction permettant d'afficher la carte carteint
        public void AffichageCarteInt()
        {
            int index = -1;
            Console.WriteLine("Conversion en entiers:");
            foreach (int elem in carteint)
            {
                index++;
                if (index % 10 == 0)
                {
                    Console.WriteLine();
                }
                Console.Write(elem);
                Console.Write(" ");
            }
            Console.WriteLine('\n');
        }



        //Fonction permettant d'afficher la carte cartedecode
        public void AffichageCarteDecode()
        {
            int x, y;
            for(x=0; x < 10; x++)
            {
                for(y=0; y < 10; y++)
                {
                    Console.Write(cartedecode[x, y]);
                    Console.Write(" ");
                    
                }
                Console.WriteLine();

            }
            
        }


        //Fonction permettant de compter le nombre de caractère de la trame
        private int CompteurCaractere(string card)
        {
            int nb = 0;
            foreach(char caractere in card)
            {
                if(caractere == '|' || caractere == ':')
                {                    
                    nb += 1;
                }
            }
            return nb;
        }

      
        public string getcartestring()
        {
            return cartebrute;
        }

        public int[] getcarteint()
        {
            return carteint;
        }

        public void setcartedecode( char[] cartedecode )
        {
            int increment = 0;
            int x = 0, y = 0;
            foreach (char element in cartedecode)
            {
                if(10 == increment)
                {
                    x++;
                    increment = 0;
                    y = 0;
                      
                }

                this.cartedecode[x, y] = element;
                increment++;
                y++;

            }
            
        }

        public char getcase(int x, int y)
        {
            return cartedecode[x,y];
        }

        public char[,] Cartedecode
        {
            get { return cartedecode; }
        }

        public void modifcartecoup(int x,int y,char joueur )
        {
            cartedecode[x, y] = joueur;
        }

        public Dictionary<char, int> Conteneurparcelle
        {
            get { return conteneurparcelle; }
        }

        /*Fonction permettant de compter le nombre de case de chaque parcelles
         * Nous pouvons donc connaitre les parcelles impaires
         */

        public Dictionary<char, int> comptageparcelle()
        {
             

            int x, y;
            char lettreincrement = 'a';
            int comptage = 0;
            

            conteneurparcelle.Clear();

            do
            {
                for (x = 0; x < 10; x++)
                {
                    for (y = 0; y < 10; y++)
                    {
                        if (cartedecode[x, y] == lettreincrement)
                        {
                            comptage++;
                        }

                    }
                }
                if (comptage != 0)
                {
                    conteneurparcelle.Add(lettreincrement, comptage);
                }
                lettreincrement++;
                comptage = 0;
            } while (lettreincrement != 'z');


            return conteneurparcelle;
        }






    }
}
