﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; // pour les outils réseau
using System.Net.Sockets; // pour les sockets


namespace La_bataille_du_café
{
    class Program
    {
       
        static void Main( string[] args )
        {
            byte[] cartebyte = new byte[300];

            //instanciation de l'objet ConnectionServeur en lui passant l'ip du serveur et le port découte
            ConnectionServeur Serveur = new ConnectionServeur("localhost", 1213);
            
            //Connection à se dernier
            Serveur.Connection();

            //Récupération de la carte
            cartebyte = Serveur.RecupCarte();

            //Console.WriteLine(Encoding.UTF8.GetString(cartebyte));

            //instanciation  de l'objet carte
            Carte carte = new Carte(cartebyte);

            //Serveur.Déconnexion();

            //Transfomation de la carte en int
            carte.DecoupageCarte();

            //carte.AffichageCarteInt();
            
            Decodage decodage = new Decodage(carte);

            //Création de la variable qui va contenir le nombre de case de chaque parcelle
            Dictionary<char, int> caseparcelles = new Dictionary<char, int>();

            CoupClient coup = new CoupClient();

            //Décodage de la carte
            decodage.AffectationChar();
            decodage.DecodageCarte();

            //Affichage de la carte décodée
            carte.AffichageCarteDecode();

            //Compatage des parcelles
            caseparcelles = carte.comptageparcelle();

            foreach (KeyValuePair<char, int> langage in caseparcelles)
            {
                Console.WriteLine("Clé: {0}, Valeur: {1}",
                    langage.Key, langage.Value);
            }


            //instanciation d'une partie
            Partie partie = new Partie(carte, coup, Serveur);

            //lancement de la partie
            partie.lancementpartie();



            Console.ReadLine();
        }

    }
}
