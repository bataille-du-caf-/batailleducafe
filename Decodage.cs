﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace La_bataille_du_café
{
    class Decodage 
    {
        private Carte CarteaTraiter;
        private char[] CarteFinale;

        public Decodage(Carte CarteaTraiter )
        {
            this.CarteaTraiter = CarteaTraiter;
            this.CarteFinale = new char[100];
        }

        //Fonction permettant de placer les Mer et les forets 
        public void AffectationChar()
        {
            int index = -1;
            int[] intarray = CarteaTraiter.getcarteint();
            foreach (int nb in intarray)
            {
                index++;
                if (nb >= 64 && nb <= 79)
                {
                    CarteFinale[index] = 'M';
                }
                else if (nb >= 32 && nb <= 47)
                {
                    CarteFinale[index] = 'F';
                }
                else
                {
                    CarteFinale[index] = 'O';
                }

            }
            
        }

        //Fonction permettant d'affecter les frontières
        private bool[] AffectationFrontiere( int nb )
        {
            bool[] frontieres = new bool[4];

            int[] bin_int = new int[4]; //nord ouest sud est
            for (int i = 0; i < 4; i++)
            {
                bin_int[i] = nb % 2;
                nb = nb / 2;
            }

            for (int i = 0; i < 4; i++)
            {

                if (bin_int[i] == 1)
                {
                    frontieres[i] = true;
                }
                else
                {
                    frontieres[i] = false;
                }

            }
            return frontieres;

        }


        //Fonction permettant de décoder la map
        public void DecodageCarte()
        {
            char lettre_parcelle = 'a';
            //Console.WriteLine();
            int indice = 0;
            int[] TableauCarteInt = CarteaTraiter.getcarteint();

            //On parcoure la map
            while (indice < 100)
            {
                //Si la case n'a pas été traité
                if (CarteFinale[indice] == 'O')
                {

                    TraitementParcelle(indice, CarteFinale, TableauCarteInt, lettre_parcelle);
                    lettre_parcelle++; //On incrémente l'indice pour passer à la lettre suivante
                    indice++; //On incrémente le bind pour passer à la case suivante
                    //Console.WriteLine(bind);
                }
                else
                {
                    indice++;
                }


            }

            CarteaTraiter.setcartedecode(CarteFinale);
        }

        //Fontion recursive
        //indice : adresse du tab ou l'on se trouve (la case que l'on traite actuellement)
        //CarteFinale : Tableau final contenant les lettres
        //TableauCarteInt : Tableau contenant les valeurs pour trouver les frontières
        //lettre_parcelle: C'est la lettre
        private void TraitementParcelle( int indice, char[] CarteFinale, int[] TableauCarteInt, char lettre_parcelle )
        {

            //affichage pas a pas
            //affichage_res(charray);

            //On look les frontière de la case ou l'on se trouve
            bool[] frontiere = AffectationFrontiere(TableauCarteInt[indice]);

            //Console.WriteLine("Frontiere : {0}{1}{2}{3}", frontiere[0], frontiere[1], frontiere[2], frontiere[3]);


            //Traitement du nord
            if ((indice > 10) && (frontiere[0] == false) && (CarteFinale[indice - 10] == 'O'))
            {

                if (CarteFinale[indice] == 'O') { CarteFinale[indice] = lettre_parcelle; }
                CarteFinale[indice - 10] = lettre_parcelle; //On met la Lettre de la case nord
                TraitementParcelle(indice - 10, CarteFinale, TableauCarteInt, lettre_parcelle);


            }
            //Tratement de l'ouest
            if (((indice - 1) % 10 != 0) && (frontiere[1] == false) && (indice > 0) && (CarteFinale[indice - 1] == 'O'))
            {

                if (CarteFinale[indice] == 'O') { CarteFinale[indice] = lettre_parcelle; }
                CarteFinale[indice - 1] = lettre_parcelle;

                TraitementParcelle(indice - 1, CarteFinale, TableauCarteInt, lettre_parcelle);


            }

            
            //traitement du sud
            if ((indice < 90) && (frontiere[2] == false) && (CarteFinale[indice + 10] == 'O'))
            {

                if (CarteFinale[indice] == 'O') { CarteFinale[indice] = lettre_parcelle; }

                CarteFinale[indice + 10] = lettre_parcelle;

                TraitementParcelle(indice + 10, CarteFinale, TableauCarteInt, lettre_parcelle);


            }

            //Traitement de l'est
            if (((indice - 9) % 10 != 0) && (frontiere[3] == false) && (indice < 100) && (CarteFinale[indice + 1] == 'O'))
            {

                if (CarteFinale[indice] == 'O') { CarteFinale[indice] = lettre_parcelle; }
                CarteFinale[indice + 1] = lettre_parcelle;

                TraitementParcelle(indice + 1, CarteFinale, TableauCarteInt, lettre_parcelle);

            }

        }


    }
}
