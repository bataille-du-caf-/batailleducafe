﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace La_bataille_du_café
{
    class CoupClient
    {
        private int xcoupprecedentjoueur;
        private int ycoupprecedentjoueur;
        private char parcellejoueur;

        private int xcoupprecedentserveur;
        private int ycoupprecedentserveur;
        private char parcelleserveur;

        /*Premier coup ou on look les parcelles impaires.
         * nous mettons le premier coup dans la parcelle impaire.
         */
        public void premiercoup(Carte carte, Dictionary<char, int> caseparcelles ,ConnectionServeur serveur)
        {
            int x, y;
            bool trouve = false;
            char impair = ' ';
            Random aleatoire = new Random();
            bool trouverand = false;

            //Mettre le foreach en commentaire si l'on veut commencer avec un nombre random pour tester mais ne correspond pas à notre stratégie
            foreach (KeyValuePair<char, int> parcelles in caseparcelles)
            {
                if (parcelles.Value % 2 != 0)
                {
                    impair = parcelles.Key;
                }
            }
            //Console.WriteLine(impair);

            if (impair == ' ')
            {
                do
                {
                    int xrand = aleatoire.Next(1, 9);
                    int yrand = aleatoire.Next(1, 9);

                    if (carte.Cartedecode[xrand, yrand] != 'M' && carte.Cartedecode[xrand, yrand] != 'F')
                    {
                        serveur.sendcoup(xrand, yrand);

                        parcellejoueur = carte.Cartedecode[xrand, yrand];
                        carte.modifcartecoup(xrand, yrand, 'J');
                        trouverand = true;
                        xcoupprecedentjoueur = xrand;
                        ycoupprecedentjoueur = yrand;
                    }
                } while (trouverand != true);

            }
            else
            {
                for (x = 0; x < 10; x++)
                {
                    for (y = 0; y < 10; y++)
                    {
                        if (carte.Cartedecode[x, y] == impair && trouve != true)
                        {
                            serveur.sendcoup(x, y);
                            parcellejoueur = carte.Cartedecode[x, y];
                            carte.modifcartecoup(x, y, 'J');
                            trouve = true;
                            xcoupprecedentjoueur = x;
                            ycoupprecedentjoueur = y;

                        }

                    }
                }
            }

            


        }

        
        //Méthode permettant de faire le coup du client. Nous réalisons les vérifications pour le placement du coup du placement le plus avantageux au moin avantageux 
        public void coupclient( Carte carte, Dictionary<char, int> caseparcelles, ConnectionServeur serveur )
        {
            bool verif = true;

            //On regarde les intersections entre le coup de l’adversaire et notre précédent coup, si l’une de ces cases fait partie d’une parcelle impaire, on joue dessus.
            verif = coupaligneimpair(carte, caseparcelles, serveur, verif);

            if (verif == true)
            {
                //On joue dans l’une des intersections valides si la condition d'avant n’est pas remplie.
                verif = coupaligne(carte, caseparcelles, serveur, verif);
            }

            if (verif == true)
            {
                //On joue dans une case valide d'une parcelle impair sur la même ligne ou colonne du précédent coup du serveur si la condition d'avant n’est pas remplie.
                verif = coupimpaire(carte, caseparcelles, serveur, verif);
            }

            if (verif == true)
            {
                //On joue dans n'importe quel case valide sur la ligne du coup du serveur si toutes les autre conditions ne sont pas remplies.
                coupsipasimpair(carte, caseparcelles, serveur, verif);
            }

        }

        //Méthode qui permet de traiter le coup du serveur et mettre à jour la carte en local
        public void coupserveur(string coupserveur,Carte carte )
        {
            xcoupprecedentserveur = Convert.ToInt32(new string(coupserveur[2], 1));
            ycoupprecedentserveur = Convert.ToInt32(new string(coupserveur[3], 1));

            parcelleserveur = carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveur];

            carte.modifcartecoup(Convert.ToInt32(new string(coupserveur[2], 1)), Convert.ToInt32(new string(coupserveur[3], 1)), 'O');
        }

        public bool coupaligneimpair( Carte carte, Dictionary<char, int> caseparcelles, ConnectionServeur serveur, bool verif )
        {
            if(carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != 'O' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != 'F' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != 'M' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != 'J' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != parcelleserveur)
            {
                if (caseparcelles[carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur]] % 2 != 0)
                {
                    serveur.sendcoup(xcoupprecedentserveur, ycoupprecedentjoueur);
                    parcellejoueur = carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur];
                    carte.modifcartecoup(xcoupprecedentserveur, ycoupprecedentjoueur, 'J');
                    return false;
                }
            }

            if (carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != 'O' && carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != 'F' && carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != 'M' && carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != 'J' && carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != parcelleserveur)
            {
                if (caseparcelles[carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur]] % 2 != 0)
                {
                    serveur.sendcoup(xcoupprecedentjoueur, ycoupprecedentserveur);
                    parcellejoueur = carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur];
                    carte.modifcartecoup(xcoupprecedentjoueur, ycoupprecedentserveur, 'J');
                    return false;
                }
            }

            return true;
        }

        public bool coupaligne( Carte carte, Dictionary<char, int> caseparcelles, ConnectionServeur serveur, bool verif )
        {
            if (carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != 'O' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != 'F' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != 'M' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != 'J' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur] != parcelleserveur)
            {

                serveur.sendcoup(xcoupprecedentserveur, ycoupprecedentjoueur);
                parcellejoueur = carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentjoueur];
                carte.modifcartecoup(xcoupprecedentserveur, ycoupprecedentjoueur, 'J');
                return false;

            }

            if (carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != 'O' && carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != 'F' && carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != 'M' && carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != 'J' && carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur] != parcelleserveur)
            {

                serveur.sendcoup(xcoupprecedentjoueur, ycoupprecedentserveur);
                parcellejoueur = carte.Cartedecode[xcoupprecedentjoueur, ycoupprecedentserveur];
                carte.modifcartecoup(xcoupprecedentjoueur, ycoupprecedentserveur, 'J');
                return false;

            }

            return true;
        }

        //Méthode qui permet de faire un coup dans case qui appartient à une parcelle impair.
        public bool coupimpaire( Carte carte, Dictionary<char, int> caseparcelles, ConnectionServeur serveur, bool verif )
        {
            int xcoupprecedentserveurlocal = xcoupprecedentserveur;
            int ycoupprecedentserveurlocal = ycoupprecedentserveur;


            //Traitement du Nord
            while (xcoupprecedentserveurlocal >= 0 && verif == true)
            {
                if (carte.Cartedecode[xcoupprecedentserveurlocal, ycoupprecedentserveur] != 'O' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'F' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'M' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'J' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != parcelleserveur)
                {
                    if (caseparcelles[carte.Cartedecode[xcoupprecedentserveurlocal, ycoupprecedentserveur]] % 2 != 0)
                    {
                        serveur.sendcoup(xcoupprecedentserveurlocal, ycoupprecedentserveur);
                        parcellejoueur = carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur];
                        carte.modifcartecoup(xcoupprecedentserveurlocal, ycoupprecedentserveur, 'J');
                        return false;
                    }
                }

                xcoupprecedentserveurlocal--;
            }

            xcoupprecedentserveurlocal = xcoupprecedentserveur;

            //Traitement du sud
            while (xcoupprecedentserveurlocal <= 9 && verif == true)
            {

                if (carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'O' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'F' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'M' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'J' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != parcelleserveur)
                {
                    if (caseparcelles[carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur]] % 2 != 0)
                    {
                        serveur.sendcoup(xcoupprecedentserveurlocal , ycoupprecedentserveur);
                        parcellejoueur = carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur];
                        carte.modifcartecoup(xcoupprecedentserveurlocal , ycoupprecedentserveur, 'J');
                        return false;
                    }
                }

                xcoupprecedentserveurlocal++;
            }

            xcoupprecedentserveurlocal = xcoupprecedentserveur;

            //Traitement de l'ouest
            while (ycoupprecedentserveurlocal >= 0 && verif == true)
            {

                if (carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != 'O' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal] != 'F' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != 'J' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != parcelleserveur && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != 'M')
                {
                    
                    if (caseparcelles[carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal]] % 2 != 0)
                    {
                        serveur.sendcoup(xcoupprecedentserveur, ycoupprecedentserveurlocal );
                        parcellejoueur = carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ];
                        carte.modifcartecoup(xcoupprecedentserveur, ycoupprecedentserveurlocal , 'J');
                        return false;
                    }
                }

                ycoupprecedentserveurlocal--;
            }

            ycoupprecedentserveurlocal = ycoupprecedentserveur;

            //Traitement de l'est
            while (ycoupprecedentserveurlocal <= 9 && verif == true)
            {
                if (carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal] != 'O' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal] != 'J' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal] != parcelleserveur && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal] != 'M' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal] != 'F')
                {
                    if (caseparcelles[carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal]] % 2 != 0)
                    {
                        serveur.sendcoup(xcoupprecedentserveur, ycoupprecedentserveurlocal);
                        parcellejoueur = carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal];
                        carte.modifcartecoup(xcoupprecedentserveur, ycoupprecedentserveurlocal, 'J');
                        return false;
                    }
                }

                ycoupprecedentserveurlocal++;
            }

            return true;
        }

        //Méthode qui permet de faire un coup dans case si on n'a pus trouver une case d'une parcelle impair avant.
        public void coupsipasimpair( Carte carte, Dictionary<char, int> caseparcelles, ConnectionServeur serveur, bool verif )
        {
            int xcoupprecedentserveurlocal = xcoupprecedentserveur;
            int ycoupprecedentserveurlocal = ycoupprecedentserveur;


            //Traitement du Nord
            while (xcoupprecedentserveurlocal >= 0 && verif == true)
            {
                if (carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'O' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'F' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'M' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'J' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != parcelleserveur)
                {

                    serveur.sendcoup(xcoupprecedentserveurlocal , ycoupprecedentserveur);
                    parcellejoueur = carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur];
                    carte.modifcartecoup(xcoupprecedentserveurlocal , ycoupprecedentserveur, 'J');
                    verif = false;

                }

                xcoupprecedentserveurlocal--;
            }

            xcoupprecedentserveurlocal = xcoupprecedentserveur;


            //Traitement du sud
            while (xcoupprecedentserveurlocal <= 9 && verif == true)
            {

                if (carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'O' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'F' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'M' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != 'J' && carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur] != parcelleserveur)
                {
                    serveur.sendcoup(xcoupprecedentserveurlocal , ycoupprecedentserveur);
                    parcellejoueur = carte.Cartedecode[xcoupprecedentserveurlocal , ycoupprecedentserveur];
                    carte.modifcartecoup(xcoupprecedentserveurlocal , ycoupprecedentserveur, 'J');
                    verif = false;
                }

                xcoupprecedentserveurlocal++;
            }

            //Traitement de l'ouest
            while (ycoupprecedentserveurlocal >= 0 && verif == true)
            {

                if (carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != 'O' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal] != 'F' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != 'J' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != parcelleserveur && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != 'M')
                {
                    serveur.sendcoup(xcoupprecedentserveur, ycoupprecedentserveurlocal );
                    parcellejoueur = carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ];
                    carte.modifcartecoup(xcoupprecedentserveur, ycoupprecedentserveurlocal , 'J');
                    verif = false;
                }

                ycoupprecedentserveurlocal--;
            }

            
            ycoupprecedentserveurlocal = ycoupprecedentserveur;

            //Traitement de l'est
            while (ycoupprecedentserveurlocal <= 9 && verif == true)
            {
                if (carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != 'O' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != 'J' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != parcelleserveur && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ] != 'M' && carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal] != 'F')
                {
                    serveur.sendcoup(xcoupprecedentserveur, ycoupprecedentserveurlocal );
                    parcellejoueur = carte.Cartedecode[xcoupprecedentserveur, ycoupprecedentserveurlocal ];
                    carte.modifcartecoup(xcoupprecedentserveur, ycoupprecedentserveurlocal , 'J');
                    verif = false;
                }

                ycoupprecedentserveurlocal++;
            }

            
        }



    }
}
