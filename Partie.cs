﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*Classe Gestion de la partie
 * Cette classe permet de mettre en place la partie et de gérer cette dernière
 */
namespace La_bataille_du_café
{
    class Partie
    {
        private Carte carte;
        private CoupClient coup;
        private ConnectionServeur serveur;

        /*Constructeur de la class */
        public Partie( Carte carte, CoupClient coup, ConnectionServeur serveur )
        {
            this.carte = carte;
            this.coup = coup;
            this.serveur = serveur;
        }

        /*Méthode du lancement de la partie. c'es tcette méthode qui va permettre de réaliser la partie*/
        public void lancementpartie()
        {
            string continus;
            Dictionary<char, int> caseparcelles = new Dictionary<char, int>();

            string coupserveur;
            string validation;
            coup.premiercoup(carte, carte.Conteneurparcelle, serveur);

            validation = serveur.coupserveur();
            Console.WriteLine("Le coup est : " + validation);
            coupserveur = serveur.coupserveur();

            continus = serveur.coupserveur();
            coup.coupserveur(coupserveur, carte);
            carte.modifcartecoup(Convert.ToInt32(new string(coupserveur[2], 1)), Convert.ToInt32(new string(coupserveur[3], 1)), 'O');
            carte.comptageparcelle();

            carte.AffichageCarteDecode();

            while (continus.Equals("ENCO") == true)
            {
                //Coup du client
                coup.coupclient(carte, carte.Conteneurparcelle, serveur);
                
                //Récupération des informations que le serveur envoie
                validation = serveur.coupserveur();
                Console.WriteLine("Le coup du client est : " + validation);
                coupserveur = serveur.coupserveur();
                
                //condition pour pouvoir gérer le cas ou le serveur ne peux plus jouer après le coup du joueur
                if(coupserveur == "FINI")
                {
                    continus = "FINI";
                    Console.WriteLine("Map après le coup du joueur : ");
                    carte.AffichageCarteDecode();

                    Console.WriteLine(continus);
                    Console.WriteLine();
                    continue;
                }
                continus = serveur.coupserveur();

                //Actualisation de la carte pour afficher le coup du serveur sur la carte en local
                coup.coupserveur(coupserveur, carte);
                
                

                //Fonction permettant de mettre à jour le nombre restant de case dans chaque parcelles
                caseparcelles = carte.comptageparcelle();

                
                Console.WriteLine("Map après le coup du serveur et du joueur : ");
                carte.AffichageCarteDecode();

                Console.WriteLine(continus);
                Console.WriteLine();

            }
            
            if(continus == "FINI")
            {
                string resultat = serveur.resultat();
                Console.WriteLine("Le score client est : " + resultat[2] + resultat[3]);
                Console.WriteLine("Le score serveur est : " + resultat[5] + resultat[6]);
            }

        }
    }
}
